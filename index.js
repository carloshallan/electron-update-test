const electron = require('electron');
const { app, BrowserWindow } = electron;
global.sharedObject = { prop1: process.argv };
const version = require('./package.json').version;
const autoUpdater = require('electron-updater');

let mainWindow;

function createWindow() {
    let args = process.argv;
    mainWindow = new BrowserWindow({ webPreferences: { webSecurity: false, nodeIntegration: true } });
    mainWindow.maximize();
    mainWindow.loadFile('./renderer/views/index.html');
    mainWindow.setTitle("ShotgunTrello " + version);
}

app.on('ready', () => {
    createWindow();
});

const sendMessage = (text) => {
    if (mainWindow) {
        mainWindow.webContents.send('message', text);
    }
}


autoUpdater.on('cheking-for-update', () => {
    sendMessage('checking updates');
})

autoUpdater.on('update-available', () => {
    sendMessage('Update available');
})

autoUpdater.on('update-not-available', () => {
    sendMessage('Update is not available');
})

autoUpdater.on('error', err => {
    sendMessage(err.toString());
})

autoUpdater.on('download-progress', progress => {
    sendMessage(`Download speed: ${progress.bytesPerSecond} - Downloaded ${progress.percent}%`)
})

autoUpdater.on('update-downloaded', () => {
    sendMessage('Update downloaded');
    autoUpdater.quitAndInstall();
})
